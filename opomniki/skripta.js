window.addEventListener("load", function() {
	// Stran naložena

	document.getElementById("prijavniGumb").addEventListener("click", function() {
		var vr = document.querySelector("#uporabnisko_ime").value;
		document.getElementById("uporabnik").innerHTML = vr;
		document.querySelector(".pokrivalo").style.visibility = "hidden";
	})
	
	var dodajOpomnik = function() {
		var nazivOpomnika = document.getElementById("naziv_opomnika").value;
		var casOpomnika = document.getElementById("cas_opomnika").value;
		document.getElementById("naziv_opomnika").value = "";
		document.getElementById("cas_opomnika").value = "";
		var opomniki = document.getElementById("opomniki");
		opomniki.innerHTML += "<div class='opomnik senca rob'><div class='naziv_opomnika'>" + nazivOpomnika + "</div><div class='cas_opomnika'> Opomnik čez <span>" + casOpomnika + "</span> sekund.</div></div>";
	};
	
	document.getElementById("dodajGumb").addEventListener("click", dodajOpomnik);
		
	// Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
		
		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML, 10);
	
			// TODO:
			if (cas == 0) {
				alert("Opomnik!\n\nZadolžitev " + opomnik.querySelector(".naziv_opomnika").innerHTML + " je potekla!");
				document.querySelector("#opomniki").removeChild(opomnik);
			} else {
				casovnik.innerHTML = cas - 1;
			}
			// - če je čas enak 0, izpiši opozorilo
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
		}
	};
	
	setInterval(posodobiOpomnike, 1000);
	
});